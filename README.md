# 🕘 IN-Class (WDD-ClassAttend)

&nbsp;&nbsp;&nbsp;&nbsp;Class check-in system by using QR Code 👉 [https://wdd-classattend.web.app/](https://wdd-classattend.web.app/)

&nbsp;&nbsp;&nbsp;&nbsp;ระบบเช็คชื่อเข้าเรียนด้วยรหัสคิวอาร์ เป็นระบบที่ทำงานบนเว็บแอปพลิเคชั่น (Web Application) สำหรับให้อาจารย์สร้างรหัสเข้าร่วมวิชาเรียนและชั้นเรียนที่สอนในแต่ละสัปดาห์ สร้างรหัสคิวอาร์ เพื่อให้นักศึกษาใช้ในการเช็คชื่อเข้าเรียน ดูประวัติการเข้าเรียนของนักศึกษาที่เช็คชื่อในแต่ละสัปดาห์ สำหรับนักศึกษาสามารถเข้าร่วมวิชาเรียน เช็คชื่อเข้าเรียน ดูประวัติการเช็คชื่อเข้าเรียน 

- สามารถเช็คชื่อผู้ที่มาเข้าเรียนเท่านั้นได้
- ผู้เรียนไม่สามารถแสกน QR code ที่เพื่อนส่งมาให้ได้ เพราะ QR code นั้นจะหมดอายุไปก่อนแล้ว และมีการสร้างภาพ QR code ขึ้นมาใหม่ทุก ๆ 5 วินาที
- รองรับการใช้งานบนบราวเซอร์ Google Chrome ทั้งใน desktop และโทรศัพท์มือถือ
- มีความรวดเร็วในการเช็คชื่อ

## 💻 Project setup

### &nbsp;&nbsp;Install Node.js

[Node.js](https://nodejs.org/) version 8.11.0 or above is recommended

### &nbsp;&nbsp;Install
```
npm install

npm install -D sass-loader 
```

### &nbsp;&nbsp;Compiles and hot-reloads for development
```
npm run serve
```
## 📑 User Guide

[Installation & User Guide](https://drive.google.com/file/d/1oUXaWR8iLhlnZ-ys4w_5liKdzjdh-POU/view?usp=sharing)

## Members

👧 60070014 Chutikarn Thanyakit [@iizcy](https://gitlab.com/iizcy)

👧 60070024 Nutpapat Youyoung [@60070024](https://gitlab.com/60070024)

👧 60070034 Thirada Theethum [@PrintTrd](https://gitlab.com/PrintTrd)



## Assistant Teacher

👩 Dr. Sirion Vittayakorn


---


Web design and development 2019

Faculty of Information Technology

King Mongkut's Institute of Technology Ladkrabang
